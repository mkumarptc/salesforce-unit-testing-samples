**Salesforce Unit Testing code samples**

This repository contains code samples which shows how to use TestFactory, Load Data from static Resource and test web service callouts.

---

## Creating Test Data

Use the files LoadingDataFromStaticResource.txt and UsingTestFactory.txt to understand how to create test data.

1. LoadingDataFromStaticResource.txt shows how to load test data from static resource.
2. UsingTestFactory.txt shows how to leverage TestFactory class to create test data.

---

## Testing Web Service callouts

Use the files UsingStaticResourceForWebServiceCallout.txt andUsingMockableForWebServicecCallout.txt to understand how to test web service callouts.

1. UsingStaticResourceForWebServiceCallout.txt shows how to use static resource as response body for web service callout testing.
2. UsingMockableForWebServicecCallout.txt shows how to use mockable interfacr to test web service callout.

---
